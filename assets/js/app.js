var profPressApp = angular.module('ProfPressApp', []);

profPressApp.controller('PostCtrl', function($scope) {
    $scope.posts = [];
    $scope.post;

    $scope.add = function() {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth();
        var monthNames = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"
        ];
        $scope.post.date = monthNames[mm] + ' ' + dd;
        $scope.posts.push(angular.copy($scope.post));
        $scope.post = {};
    }
});
